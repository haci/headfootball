package com.mobaxe.headfootball;

import com.badlogic.gdx.Game;
import com.mobaxe.headfootball.helpers.Assets;
import com.mobaxe.headfootball.managers.ScreenManager;
import com.mobaxe.headfootball.screens.MyScreens;

public class HeadFootball extends Game {

	public static ActionResolver actionResolver;

	@Override
	public void create() {

		Assets.loadTexturesOnCreate();
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
	}


	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
		Assets.dispose();
	}
}
