package com.mobaxe.headfootball.managers;

import java.util.ArrayList;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mobaxe.headfootball.helpers.Assets;
import com.mobaxe.headfootball.helpers.BoxProperties;
import com.mobaxe.headfootball.objects.Ball;
import com.mobaxe.headfootball.objects.Player1;
import com.mobaxe.headfootball.objects.Player2;
import com.mobaxe.headfootball.ui.JumpButton;
import com.mobaxe.headfootball.ui.LeftButton;
import com.mobaxe.headfootball.ui.RightButton;
import com.mobaxe.headfootball.utils.Utils;

public class GameManager {

	public static OrthographicCamera camera;
	public static InputMultiplexer multiplexer;
	public static SpriteBatch batch;
	public static Stage stage;
	public static World world;
	private static ArrayList<Table> btnTables;

	public static Player1 player1;
	public static Player2 player2;
	public static Ball ball;
	public static BoxProperties ground;
	public static BoxProperties upLine;
	public static BoxProperties middleLine;
	public static BoxProperties leftLine;
	public static BoxProperties rightLine;

	public static boolean isJumpClicked;
	public static boolean isRightClicked;
	public static boolean isLeftClicked;
	private static LeftButton left;
	private static RightButton right;
	private static JumpButton jump;

	public static void init(World w, OrthographicCamera c, Stage s, SpriteBatch sb, InputMultiplexer im) {
		world = w;
		camera = c;
		stage = s;
		batch = sb;
		multiplexer = im;
	}

	public static void initWorld() {
		ground = new BoxProperties(world, camera.viewportWidth, 4f, new Vector2(0, -9), BodyType.StaticBody,
				false, Utils.GROUND);
		middleLine = new BoxProperties(world, 0.2f, camera.viewportHeight - 5f, new Vector2(0, 2),
				BodyType.StaticBody, false, Utils.MIDLINE);
		leftLine = new BoxProperties(world, 0.2f, camera.viewportHeight - 5f, new Vector2(-18, 2),
				BodyType.StaticBody, false, Utils.LEFTLINE);
		rightLine = new BoxProperties(world, 0.2f, camera.viewportHeight - 5f, new Vector2(18, 2),
				BodyType.StaticBody, false, Utils.RIGHTLINE);
		upLine = new BoxProperties(world, camera.viewportWidth, 0.2f, new Vector2(0, 12),
				BodyType.StaticBody, false, Utils.UPLINE);

		createButtons();
		createBall();
		createPlayer1();
		createPlayer2();

	}

	public static void createPlayer1() {
		player1 = new Player1(world, new Vector2(-10, -2), Assets.player1);
	}

	public static void createPlayer2() {
		player2 = new Player2(world, new Vector2(10, -2), Assets.player1);
	}

	public static void createBall() {
		ball = new Ball(world, new Vector2(0, 9), Assets.player1);
	}

	public static void createButtons() {
		btnTables = new ArrayList<Table>();
		Table tbl1 = new Table();
		Table tbl2 = new Table();
		Table tbl3 = new Table();
		btnTables.add(tbl1);
		btnTables.add(tbl2);
		btnTables.add(tbl3);

		left = new LeftButton();
		right = new RightButton();
		jump = new JumpButton();

		for (int i = 0; i < btnTables.size(); i++) {
			if (i == 0) {
				btnTables.get(i).add(left).pad(0, 150, 200, 0);
			} else if (i == 1) {
				btnTables.get(i).add(jump).pad(0, 1400, 105, 0);
			} else {
				btnTables.get(i).add(right).pad(0, 300, 105, 0);
			}
		}
		for (Table table : btnTables) {
			stage.addActor(table);
		}
	}

	public static void updateStages(float delta) {
		stage.act(delta);
		stage.draw();
	}

	public static void updateSprites(float delta) {
		batch.setProjectionMatrix(camera.combined);

		// PLAYER1
		batch.begin();
		player1.draw(batch);
		batch.end();
		// PLAYER2
		batch.begin();
		player2.draw(batch);
		batch.end();
		// BALL
		batch.begin();
		ball.draw(batch);
		batch.end();

	}

	public static void createContactListener() {

		world.setContactListener(new ContactListener() {
			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {

			}

			@Override
			public void endContact(Contact contact) {
				player1.setOnGround(false);
			}

			@Override
			public void beginContact(Contact contact) {
				Body bodyA = contact.getFixtureA().getBody();
				Body bodyB = contact.getFixtureB().getBody();
				if (bodyB.equals(player1.body) && bodyA.equals(ground.getBody())) {
					player1.setOnGround(true);
				}

			}
		});
	}

}
