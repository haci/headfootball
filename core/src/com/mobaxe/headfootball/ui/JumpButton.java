package com.mobaxe.headfootball.ui;


import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.headfootball.helpers.Assets;
import com.mobaxe.headfootball.managers.GameManager;

public class JumpButton extends Button {

	private String buttonUp;
	private String buttonDown;
	private Skin skin;
	private ButtonStyle style;

	public JumpButton() {
		buttonUp = "ButtonUp";
		buttonDown = "ButtonDown";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, Assets.loadTexture("images/jump.png"));
		skin.add(buttonDown, Assets.loadTexture("images/jactive.png"));
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.down=skin.getDrawable(buttonDown);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				GameManager.isJumpClicked = true;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				GameManager.isJumpClicked = false;
			}

		});
	}
}
