package com.mobaxe.headfootball.screens;
import com.badlogic.gdx.Screen;

public enum MyScreens {

	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen();
		}
	},
	SPLASH_SCREEN {
		public Screen getScreenInstance() {
			return new SplashScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
