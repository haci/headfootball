package com.mobaxe.headfootball.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.utils.Disposable;
import com.mobaxe.headfootball.helpers.Box2DFactory;
import com.mobaxe.headfootball.managers.GameManager;
import com.mobaxe.headfootball.utils.Utils;

public class Player2 implements Disposable {

	public enum STATE {
		OFFENSIVE, DEFENSIVE
	}

	public Body body;
	public Body head;
	public Sprite sprite;
	private World world;
	private STATE state;
	private Ball ball;
	private Vector2 destination = new Vector2();
	private Vector2 speed = new Vector2(0, -1);

	public Player2(World world, Vector2 pos, Texture player) {

		sprite = new Sprite(player);
		sprite.setSize(1f, 1f);
		sprite.setOriginCenter();

		this.world = world;

		Shape shape = Box2DFactory.createCircleShape(1.1f);
		FixtureDef fixtureDef = Box2DFactory.createFixture(shape, 0.8f, 0.1f, 0.1f, false);
		fixtureDef.filter.groupIndex = Utils.PLAYER2;
		Body body = Box2DFactory.createBody(world, BodyType.DynamicBody, fixtureDef, pos, false);
		this.body = body;
		this.body.setGravityScale(4);

		Shape sh = Box2DFactory.createCircleShape(1.6f);
		FixtureDef fd = Box2DFactory.createFixture(sh, 0.1f, 0.1f, 0.1f, false);
		fd.filter.groupIndex = Utils.PLAYER2;
		Body bdy = Box2DFactory.createBody(world, BodyType.DynamicBody, fd, body.getPosition(), false);
		bdy.setFixedRotation(true);
		this.head = bdy;
		this.head.setGravityScale(4);

		RevoluteJointDef rDef = new RevoluteJointDef();
		rDef.bodyA = this.head;
		rDef.bodyB = this.body;
		rDef.collideConnected = true;
		rDef.localAnchorA.y = -1.6f;
		world.createJoint(rDef);

		state = STATE.DEFENSIVE;
		ball = GameManager.ball;

	}

	public void draw(SpriteBatch batch) {
		move(batch);

	}

	private void move(SpriteBatch batch) {
		Vector2 currentVelocity = body.getLinearVelocity();

		switch (ball.getState()) {
		case AI:
			state = STATE.OFFENSIVE;
			break;
		case PLAYER:
			state = STATE.DEFENSIVE;
			break;
		default:
			state = STATE.DEFENSIVE;
			break;
		}

		if (state == STATE.DEFENSIVE) {
			destination.set(10, 0);
		} else {
			destination.set(ball.getBody().getPosition().x, ball.getBody().getPosition().y);
		}
		if (body.getPosition().x - 1.1f <= -Utils.widthMeters / 2
				|| body.getPosition().x + 1.1f >= Utils.widthMeters / 2)
			currentVelocity.x = currentVelocity.x * -0.8f;

		boolean increaseX = destination.x > body.getPosition().x;

		if (increaseX)
			currentVelocity.x = Math.min(currentVelocity.x + 1.2f, 11.0f);
		else
			currentVelocity.x = Math.max(currentVelocity.x - 1.2f, -11.0f);

		body.applyForce(new Vector2(100, 0), ball.getBody().getPosition(), true);

		if (body.getPosition().y + 15 > ball.getBody().getPosition().y
				&& body.getPosition().y + 5 < ball.getBody().getPosition().y
				&& ball.getX() > body.getPosition().x - 1 && ball.getX() < body.getPosition().x + 1) {
			body.applyForce(new Vector2(0, 5000), ball.getBody().getPosition(), true);
		} else {
			body.applyForce(new Vector2(0, 0), ball.getBody().getPosition(), true);
		}

		setSpeed(currentVelocity.x, currentVelocity.y);
		updateSpeed(ball);

		if (Utils.heightMeters >= body.getPosition().y) {
			currentVelocity.x = 0;
		}
	}

	private void updateSpeed(Ball ball) {
		body.setLinearVelocity(speed);
	}

	public void switchStatus() {
		if (state == STATE.DEFENSIVE)
			state = STATE.OFFENSIVE;
		else
			state = STATE.DEFENSIVE;
	}

	public Vector2 getSpeed() {
		return speed;
	}

	public void setSpeed(float x, float y) {
		this.speed.x = x;
		this.speed.y = y;
	}

	public Vector2 getDestination() {
		return destination;
	}

	public void setDestination(Vector2 destination) {
		this.destination = destination;
	}

	@Override
	public void dispose() {
		world.destroyBody(body);
	}

}
