package com.mobaxe.headfootball.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.utils.Disposable;
import com.mobaxe.headfootball.helpers.Box2DFactory;
import com.mobaxe.headfootball.managers.GameManager;
import com.mobaxe.headfootball.utils.Utils;

public class Player1 implements Disposable {

	public Body body;
	public Body head;
	public Sprite sprite;
	private World world;
	// private Sprite stop;
	private int remainingJumpSteps;
	private boolean onGround;
	private boolean moving;
	private int direction;// -1 LEFT 1 RIGHT

	public Player1(World world, Vector2 pos, Texture player) {

		sprite = new Sprite(player);
		sprite.setSize(1f, 1f);
		sprite.setOriginCenter();

		this.world = world;

		Shape shape = Box2DFactory.createCircleShape(1.1f);
		FixtureDef fixtureDef = Box2DFactory.createFixture(shape, 0.8f, 0.1f, 0.1f, false);
		fixtureDef.filter.groupIndex = Utils.PLAYER1;
		Body body = Box2DFactory.createBody(world, BodyType.DynamicBody, fixtureDef, pos, false);
		this.body = body;
		this.body.setGravityScale(4);

		Shape sh = Box2DFactory.createCircleShape(1.6f);
		FixtureDef fd = Box2DFactory.createFixture(sh, 0.1f, 0.1f, 0.1f, false);
		fd.filter.groupIndex = Utils.PLAYER1;
		Body bdy = Box2DFactory.createBody(world, BodyType.DynamicBody, fd, body.getPosition(), false);
		bdy.setFixedRotation(true);
		this.head = bdy;
		this.head.setGravityScale(4);

		RevoluteJointDef rDef = new RevoluteJointDef();
		rDef.bodyA = this.head;
		rDef.bodyB = this.body;
		rDef.collideConnected = true;
		rDef.localAnchorA.y = -1.6f;
		world.createJoint(rDef);

	}

	public void draw(SpriteBatch batch) {
		handleInputs();
		handleSprites();
		jump(batch);
		move(batch);

	}

	private void move(SpriteBatch batch) {
		if (isMoving()) {
			setMoving(false);
			Vector2 velocity = body.getLinearVelocity();
			if (direction == -1) {
				velocity.x = -6.5f;
			} else {
				velocity.x = 6.5f;
			}
			body.setLinearVelocity(velocity.x, velocity.y);

			sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2,
					body.getPosition().y - sprite.getHeight() / 3.8f);
			sprite.draw(batch);

		}
	}

	private void jump(SpriteBatch batch) {
		if (remainingJumpSteps > 0) {
			body.applyForce(new Vector2(0, 1500), body.getWorldCenter(), true);
			remainingJumpSteps--;

			// stop.setPosition(body.getPosition().x - stop.getWidth() / 2,
			// body.getPosition().y - stop.getHeight() / 3.8f);
			// stop.draw(batch);
			setOnGround(false);

		} else {
			if (isMoving() == false) {
				// stop.setPosition(body.getPosition().x - stop.getWidth() / 2,
				// body.getPosition().y - stop.getHeight() / 3.8f);
				// stop.draw(batch);
				Vector2 velocity = body.getLinearVelocity();
				velocity.x = 0;
				body.setLinearVelocity(velocity.x, velocity.y);
			}
		}
	}

	private void handleSprites() {
		if (direction == -1) {
			sprite.setScale(-1, 1);
			// stop.setScale(-1, 1);
		} else {
			sprite.setScale(1, 1);
			// stop.setScale(1, 1);
		}
	}

	private void handleInputs() {
		if (isOnGround()) {
			if (Gdx.input.isKeyPressed(Keys.SPACE)) {
				remainingJumpSteps = 6;
			}
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			setMoving(true);
			direction = 1;
		}
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			setMoving(true);
			direction = -1;
		}
		if (isOnGround()) {
			if (GameManager.isJumpClicked) {
				remainingJumpSteps = 6;
			}
		}
		if (GameManager.isRightClicked) {
			setMoving(true);
			direction = 1;
		}
		if (GameManager.isLeftClicked) {
			setMoving(true);
			direction = -1;
		}

	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
	}

	@Override
	public void dispose() {
		world.destroyBody(body);
	}

}
