package com.mobaxe.headfootball.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.mobaxe.headfootball.helpers.Box2DFactory;
import com.mobaxe.headfootball.utils.Utils;

public class Ball {

	public enum STATE {
		PLAYER, AI, CENTER
	}

	public Sprite sprite;
	private World world;
	private int direction;// -1 LEFT 1 RIGHT
	private Body body;
	private float radius = 1.1f;
	private STATE state;

	public Ball(World world, Vector2 pos, Texture texture) {

		sprite = new Sprite(texture);
		sprite.setSize(1f, 1f);
		sprite.setOriginCenter();

		Shape shape = Box2DFactory.createCircleShape(radius);
		FixtureDef fixtureDef = Box2DFactory.createFixture(shape, 0.1f, 1.2f, 1.2f, false);
		fixtureDef.filter.groupIndex = Utils.BALL;
		Body body = Box2DFactory.createBody(world, BodyType.DynamicBody, fixtureDef, pos, false);
		this.body = body;
		this.body.setLinearDamping(0.5f);
		state = STATE.CENTER;
	}

	public void draw(SpriteBatch batch) {
		switchState();
	}

	public void switchState() {
		if (getX() > 0) {
			state = STATE.AI;
		} else if (getX() < 0) {
			state = STATE.PLAYER;
		} else {
			state = STATE.CENTER;
		}
	}

	public STATE getState() {
		return state;
	}

	public Body getBody() {
		return body;
	}

	public float getX() {
		return body.getPosition().x;
	}

	public float getY() {
		return body.getPosition().y;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public float getRadius() {
		return radius;
	}
}
